export const environment = {
  production: true,
  tmdbApiKey: '6e79657f6923cd907713dbcac5853f49',
  tmdbApiURL: 'https://api.themoviedb.org/3/',
  imgBaseUrl: 'https://www.themoviedb.org/t/p/original'
};
